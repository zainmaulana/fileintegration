<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KoneksiDB extends Model
{
    //
    protected $table = 'tblm_koneksidb';

    protected $primaryKey = 'id_koneksidb';

    protected $fillable = ['nama_layanan',
    'name_db_conn',
    'schema_db',
    'server_db',
    'port_db',
    'app_db_uname',
    'app_db_pass'
    ];

    const CREATED_AT = null;
    const UPDATED_AT = 'last_updated_on';
}

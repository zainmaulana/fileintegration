<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\MasterDataController;

use Carbon\Carbon;

class FileExport implements FromCollection, WithHeadings
{

    protected $dbcon_name;
    protected $export_data;

    public function __construct($dbcon_name, $export_data) {

        $this->dbcon_name = $dbcon_name;
        $this->export_data = $export_data;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //Query Execution
        $query = DB::connection($this->dbcon_name)
        ->select($this->export_data->query_export);

        return collect($query);
    }

    public function headings(): array
    {
        //Kode File
        $kode_file = substr($this->export_data->nama_export,0,3);

        //Set GUID Auto
        $guid_a = Str::uuid();
        $guid_a = str_replace('-','',$guid_a);
        $guid_a = strtoupper($guid_a);

        //Set GUID Manual
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $guid_m = substr($charid, 0, 8)
            .substr($charid, 8, 4)
            .substr($charid, 12, 4)
            .substr($charid, 16, 4)
            .substr($charid, 20, 12);// "}"

        //Set Company Code
        $company = 'HK01';

        //Get Date Now
        $controller = new MasterDataController;
        $get_date_file = $controller->get_date_file();

        $year = substr($get_date_file,0,4);
        $month = substr($get_date_file,4,2);
        $day = substr($get_date_file,6,2);
        $date_now = $year.'.'.$month.'.'.$day;

        $hour = substr($get_date_file,8,2);
        $minute = substr($get_date_file,10,2);
        $second = substr($get_date_file,12,2);
        $time_now = $hour.':'.$minute.':'.$second;

        //Splitting Header
        $get_header = explode(',',$this->export_data->coll_fields);

        foreach ($get_header as $header) {
            $headers[] = $header;
        }

        return [
            [$kode_file, $guid_a, $company, $date_now, $time_now],
            // ['no', 'nama', 'email', 'password', 'created_date', 'updated_date'],
            $headers
        ];
    }
}

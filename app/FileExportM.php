<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileExportM extends Model
{
    //
    protected $table = 'tblm_file_export';

    protected $primaryKey = 'id_file_export';

    protected $fillable = ['id_koneksidb',
    'nama_layanan',
    'nama_export',
    'prefix_name',
    'sftp_name',
    'query_export',
    'coll_fields'
    ];

    const CREATED_AT = null;
    const UPDATED_AT = 'last_updated_on';
}

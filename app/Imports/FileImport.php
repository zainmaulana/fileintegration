<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Carbon\Carbon;

class FileImport implements ToCollection, WithHeadingRow
{

    protected $dbcon_name;
    protected $import_data;

    public function __construct($dbcon_name, $import_data) {

        $this->dbcon_name = $dbcon_name;
        $this->import_data = $import_data;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        //Splitting Header
        $get_header = explode(',',$this->import_data->coll_fields);

        //Query Execution
        foreach ($rows as $row) {
            foreach ($get_header as $header) {
                DB::connection($this->dbcon_name)
                ->table($this->import_data->table_source)
                ->where($this->import_data->pk_field,'=',$row[$this->import_data->pk_field])
                ->update(
                    [
                        $header => $row[$header]
                    ]
                );
            }
        }
    }

    public function headingRow(): int
    {
        return 2;
    }
}

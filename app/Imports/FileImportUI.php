<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Carbon\Carbon;

class FileImportUI implements ToCollection, WithHeadingRow
{

    protected $dbcon_name;
    protected $importui_data;

    public function __construct($dbcon_name, $importui_data) {

        $this->dbcon_name = $dbcon_name;
        $this->importui_data = $importui_data;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        //Splitting Header
        $get_header = explode(',',$this->importui_data->coll_fields);

        //Query Execution
        foreach ($rows as $row) {
            $checkRow = DB::connection($this->dbcon_name)
            ->table($this->importui_data->table_source)
            ->where($this->importui_data->pk_field,'=',$row[$this->importui_data->pk_field])
            ->first();

            if($checkRow){
                foreach ($get_header as $header) {
                    DB::connection($this->dbcon_name)
                    ->table($this->importui_data->table_source)
                    ->where($this->importui_data->pk_field,'=',$row[$this->importui_data->pk_field])
                    ->update(
                        [
                            $header => $row[$header]
                        ]
                    );
                }
            }
            else{
                DB::connection($this->dbcon_name)
                ->table($this->importui_data->table_source)
                ->insert(
                    [
                        $this->importui_data->pk_field => $row[$this->importui_data->pk_field],
                    ]
                );

                foreach ($get_header as $header) {
                    DB::connection($this->dbcon_name)
                    ->table($this->importui_data->table_source)
                    ->where($this->importui_data->pk_field,'=',$row[$this->importui_data->pk_field])
                    ->update(
                        [
                            $header => $row[$header]
                        ]
                    );
                }
            }
        }
    }

    public function headingRow(): int
    {
        return 1;
    }
}

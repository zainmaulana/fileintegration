<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FileExportM;
use App\FileImportM;
use App\FileImportUIM;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fexport = FileExportM::all();

        $fimport = FileImportM::all();

        $fimport2 = FileImportM::all();

        $fimportui = FileImportUIM::all();

        return view('home', compact('fexport', 'fimport', 'fimport2', 'fimportui'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

use App\Http\Controllers\MasterDataController;
use App\Imports\FileImport;
use App\FileImportM;
use App\MonitoringAktivitas;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Get DB Connection
    public function get_db_conn(int $id_dbcon){

        $dbcon_name = 'db_conn_'.$id_dbcon;

        $controller = new MasterDataController;
        $app_con = $controller->get_dbcon_app($id_dbcon);

        //Config All DB
        config(['database.connections.'.$dbcon_name => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => $app_con->server_db,
            'port' => $app_con->port_db,
            'database' => $app_con->name_db_conn,
            'username' => $app_con->app_db_uname,
            'password' => Crypt::decryptString($app_con->app_db_pass),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => $app_con->schema_db,
            'sslmode' => 'prefer',
        ]]);

        return $dbcon_name;
    }

    //Get Data Import per App
    public function post_import(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_file_import' => 'required'
        ]);

        $import_data = FileImportM::where('id_file_import','=',$data->id_file_import)
        ->first();

        $this->import_csv($this->get_db_conn($import_data->id_koneksidb), $import_data);

        return redirect('/');
    }

    public function post_import_manual(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_file_import2' => 'required',
            'file_import_search' => 'required'
        ]);

        $file_import = explode('|', $data->id_file_import2);

        $import_data = FileImportM::where('id_file_import','=',$file_import[0])
        ->first();

        $this->import_csv_manual($this->get_db_conn($import_data->id_koneksidb), $import_data, $data->file_import_search);

        return redirect('/');
    }

    //Get Data Import per App by Command
    public function post_import_cmd(int $id_file_import){

        $import_data = FileImportM::where('id_file_import','=',$id_file_import)
        ->first();

        $this->import_csv($this->get_db_conn($import_data->id_koneksidb), $import_data);

        return redirect('/');
    }

    //Import CSV
    public function import_csv(String $dbcon_name, Object $import_data){

        $prefix_name = $import_data->prefix_name;

        $kode_file = substr($import_data->nama_import,0,3);

        $get_activity = DB::table('tblt_monitoring as A')
        ->join('tblm_file_export as B','A.id_filein','=','B.id_file_export')
        ->where('status_filein','=','Exported')
        ->whereRaw('substring(nama_export for 3) = ?',$kode_file)
        ->orderBy('id_monitoring','desc')
        ->first();

        if($get_activity){
            //Cek Source
            if (strpos($import_data->prefix_name,"-")) {
                $file = $prefix_name.$get_activity->nama_file;
            }
            else{
                $file = $get_activity->nama_file;
            }

            Excel::import(new FileImport($dbcon_name, $import_data), $file, $import_data->sftp_name, \Maatwebsite\Excel\Excel::CSV, [
                'Content-Type' => 'text/csv',
            ]);

            $this->post_import_activity($get_activity->id_monitoring, $import_data->id_file_import);
        }
        else{
            return redirect()->back()->with('error', 'Tidak ada File');
        }

        return redirect('/')->with('success', 'Import Success');
    }

    public function import_csv_manual(String $dbcon_name, Object $import_data, String $nama_file){

        $prefix_name = $import_data->prefix_name;

        $get_activity = MonitoringAktivitas::where('nama_file','=',$nama_file)
        ->first();

        if($get_activity){
            //Cek Source
            if (strpos($import_data->prefix_name,"-")) {
                $file = $prefix_name.$get_activity->nama_file;
            }
            else{
                $file = $get_activity->nama_file;
            }

            Excel::import(new FileImport($dbcon_name, $import_data), $file, $import_data->sftp_name, \Maatwebsite\Excel\Excel::CSV, [
                'Content-Type' => 'text/csv',
            ]);

            $this->post_import_activity($get_activity->id_monitoring, $import_data->id_file_import);
        }
        else{
            return redirect()->back()->with('error', 'Tidak ada File');
        }

        return redirect('/')->with('success', 'Import Success');
    }

    //Save Aktivitas Import
    public function post_import_activity(int $id, int $id_filein_import){

        $monak = MonitoringAktivitas::find($id);
        $monak->id_filein_import = $id_filein_import;
        $monak->status_filein = 'Imported';
        $monak->last_updated_on = Carbon::now('Asia/Jakarta');
        $monak->save();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

use App\Http\Controllers\MasterDataController;
use App\Imports\FileImportUI;
use App\FileImportUIM;
use App\MonitoringAktivitas;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ImportUIController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Get DB Connection
    public function get_db_conn(int $id_dbcon){

        $dbcon_name = 'db_conn_'.$id_dbcon;

        $controller = new MasterDataController;
        $app_con = $controller->get_dbcon_app($id_dbcon);

        //Config All DB
        config(['database.connections.'.$dbcon_name => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => $app_con->server_db,
            'port' => $app_con->port_db,
            'database' => $app_con->name_db_conn,
            'username' => $app_con->app_db_uname,
            'password' => Crypt::decryptString($app_con->app_db_pass),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => $app_con->schema_db,
            'sslmode' => 'prefer',
        ]]);

        return $dbcon_name;
    }

    //Get Data Import per App
    public function post_importui(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_file_importui' => 'required'
        ]);

        $importui_data = FileImportUIM::where('id_file_importui','=',$data->id_file_importui)
        ->first();

        $this->importui_csv($this->get_db_conn($importui_data->id_koneksidb), $importui_data);

        return redirect('/');
    }

    public function post_importui_manual(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_file_importui2' => 'required',
            'file_importui_search' => 'required'
        ]);

        $file_importui = explode('|', $data->id_file_importui2);

        $importui_data = FileImportUIM::where('id_file_importui','=',$file_importui[0])
        ->first();

        $this->importui_csv_manual($this->get_db_conn($importui_data->id_koneksidb), $importui_data, $data->file_importui_search);

        return redirect('/');
    }

    //Get Data Import per App by Command
    public function post_importui_cmd(int $id_file_importui){

        $importui_data = FileImportUIM::where('id_file_importui','=',$id_file_importui)
        ->first();

        $this->importui_csv($this->get_db_conn($importui_data->id_koneksidb), $importui_data);

        return redirect('/');
    }

    //Import CSV
    public function importui_csv(String $dbcon_name, Object $importui_data){

        $prefix_name = $importui_data->prefix_name;

        //Cek Source
        $controller = new MasterDataController;
        $get_date_file = $controller->get_date_file();

        $date_now = substr($get_date_file,0,8);
        $time_now = substr($get_date_file,8,6);
        $date_file = $date_now.'_'.$time_now;

        // $file = $prefix_name.'_'.$date_now.'.csv';

        //Cek Source Auto
        $sftp_name = explode(',',$importui_data->sftp_name);
        $allfiles = Storage::disk($sftp_name[0])->files($importui_data->prefix_name);

        $file = explode('/',end($allfiles));

        if($file){

            Excel::import(new FileImportUI($dbcon_name, $importui_data), $file[1], $sftp_name[1], \Maatwebsite\Excel\Excel::CSV, [
                'Content-Type' => 'text/csv',
            ]);

            $this->post_importui_activity($importui_data->id_file_importui, $file[1]);
        }
        else{
            return redirect()->back()->with('error', 'Tidak ada File');
        }

        return redirect('/')->with('success', 'Import Success');
    }

    public function importui_csv_manual(String $dbcon_name, Object $importui_data, String $nama_file){

        $prefix_name = $importui_data->prefix_name;

        $get_activity = MonitoringAktivitas::where('nama_file','=',$nama_file)
        ->first();

        if($get_activity){
            //Cek Source
            if (strpos($importui_data->prefix_name,"-")) {
                $file = $prefix_name.$get_activity->nama_file;
            }
            else{
                $file = $get_activity->nama_file;
            }

            Excel::import(new FileImportUI($dbcon_name, $importui_data), $file, $importui_data->sftp_name, \Maatwebsite\Excel\Excel::CSV, [
                'Content-Type' => 'text/csv',
            ]);

            $this->post_importui_activity($get_activity->id_monitoring, $importui_data->id_file_importui);
        }
        else{
            return redirect()->back()->with('error', 'Tidak ada File');
        }

        return redirect('/')->with('success', 'Import Success');
    }

    //Save Aktivitas Import
    public function post_importui_activity(int $id_filein_importui, String $nama_file){

        MonitoringAktivitas::create([
            'id_filein_import' => $id_filein_importui,
            'nama_file' => $nama_file,
            'status_filein' => 'Imported Only',
            'created_on' => Carbon::now('Asia/Jakarta'),
            'last_updated_on' => Carbon::now('Asia/Jakarta')
        ]);
    }
}

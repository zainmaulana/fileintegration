<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

use App\Http\Controllers\MasterDataController;
use App\Exports\FileExport;
use App\FileExportM;
use App\MonitoringAktivitas;

use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ExportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Get DB Connection
    public function get_db_conn(int $id_dbcon){

        $dbcon_name = 'db_conn_'.$id_dbcon;

        $controller = new MasterDataController;
        $app_con = $controller->get_dbcon_app($id_dbcon);

        //Config All DB
        config(['database.connections.'.$dbcon_name => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => $app_con->server_db,
            'port' => $app_con->port_db,
            'database' => $app_con->name_db_conn,
            'username' => $app_con->app_db_uname,
            'password' => Crypt::decryptString($app_con->app_db_pass),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => $app_con->schema_db,
            'sslmode' => 'prefer',
        ]]);

        return $dbcon_name;
    }

    //Get Data Export per App
    public function post_export(Request $data){

        //Validate Request
        $this->validate($data,[
            'id_file_export' => 'required'
        ]);

        $export_data = FileExportM::where('id_file_export','=',$data->id_file_export)
        ->first();

        $this->export_csv($this->get_db_conn($export_data->id_koneksidb), $export_data);

        return redirect('/')->with('success', 'Export Success');
    }

    //Get Data Export per App by Command
    public function post_export_cmd(int $id_file_export){

        $export_data = FileExportM::where('id_file_export','=',$id_file_export)
        ->first();

        $this->export_csv($this->get_db_conn($export_data->id_koneksidb), $export_data);

        return redirect('/')->with('success', 'Export Success');
    }

    //Export CSV
    public function export_csv(String $dbcon_name, Object $export_data){

        $prefix_name = $export_data->prefix_name;

        $controller = new MasterDataController;
        $get_date_file = $controller->get_date_file();

        $date_now = substr($get_date_file,0,8);
        $time_now = substr($get_date_file,8,6);
        $date_file = $date_now.'_'.$time_now;

        $file = $prefix_name.'_'.$date_file.'.csv';

        $select_result = DB::connection($dbcon_name)
        ->select($export_data->query_export);

        //Cek Null Query
        if($select_result == []){
            $this->post_export_activity($export_data->id_file_export, 'Tidak Ada Vendor yang dikirim', 'Not Exported');
        }
        else{
            Excel::store(new FileExport($dbcon_name, $export_data), 'csv/'.$file, '', \Maatwebsite\Excel\Excel::CSV, [
                'Content-Type' => 'text/csv',
            ]);

            $this->store_csv_public($file);

            $this->store_to_sftp($file, $file, $export_data->sftp_name);

            $this->post_export_activity($export_data->id_file_export, $file, 'Exported');
        }
    }

    //Save Aktivitas Export
    public function post_export_activity(int $id_filein, String $nama_file, String $status_exp){

        MonitoringAktivitas::create([
            'id_filein' => $id_filein,
            'nama_file' => $nama_file,
            'status_filein' => $status_exp,
            'created_on' => Carbon::now('Asia/Jakarta'),
            'last_updated_on' => Carbon::now('Asia/Jakarta')
        ]);
    }

    //Storing Files to Public
    public function store_csv_public(String $file){

        $file_ori = 'csv/'.$file;

        $file_dest = 'public/'.$file;

        $file_exists = Storage::exists($file_dest);

        if($file_exists){

            Storage::delete($file_dest);

            return Storage::move($file_ori, $file_dest);
        }

        else{
            return Storage::move($file_ori, $file_dest);
        }
    }

    //Storing Files to FTP
    public function store_to_sftp(String $file, String $stored_file, String $sftp_name){

        $file_ori = 'public/'.$file;

        $contents = Storage::get($file_ori);

        return Storage::disk($sftp_name)->put($stored_file, $contents);
    }
}

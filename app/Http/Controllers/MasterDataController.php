<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\KoneksiDB;
use App\FileExportM;
use App\FileImportM;
use App\FileImportUIM;
use App\MonitoringAktivitas;

use Carbon\Carbon;

class MasterDataController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    }

    //User
    public function list_user(){

        $user = User::all();

    	return view('masterdata.user', compact('user'));
    }

    public function insert_user(Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:8'
        ]);

        //Post Request
        $user = User::create([
            'name' => $data->name,
            'email' => $data->email,
            'password' => Hash::make($data->password)
        ]);

    	return redirect('/masterdata/user');
    }

    public function update_user($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id
        ]);

        //Update Request
        $user = User::find($id);
        $user->name = $data->name;
        $user->email = $data->email;

        if($data->password <> NULL){
            if($data->password_confirmation == $data->password){
                $user->password = Hash::make($data->password);
            }
            else{
                return redirect()->back()->with('error', 'Konfirmasi Password tidak sama');
            }
        }
        elseif($data->password == NULL || $data->password == ''){

        }

        $user->save();

        return redirect('/masterdata/user');
    }

    public function delete_user($id){

        $user = User::find($id);
        $user->delete();

        return redirect('/masterdata/user');
    }

    //Koneksi DB
    public function get_dbcon_all(){

        $assetk_cons = DB::connection('helpdesk')
        ->table('tblm_semisso as A')
        ->leftJoin('tblm_asset_kategori as B','A.id_layanan','=','B.id_asset_kategori')
        ->select('A.id_semisso',
        'B.id_asset_kategori',
        'B.nama_kategori',
        'B.nama_sub_kategori',
        'B.nama_komponen')
        ->where('B.nama_kategori','=','Aplikasi')
        ->orderBy('B.nama_kategori','asc')
        ->orderBy('B.nama_sub_kategori','asc')
        ->orderBy('B.nama_komponen','asc')
        ->get();

        return $assetk_cons;
    }

    public function get_dbcon_app(int $id_dbcon){

        $assetk_con = DB::connection('helpdesk')
        ->table('tblm_semisso as A')
        ->leftJoin('tblm_asset_kategori as B','A.id_layanan','=','B.id_asset_kategori')
        ->select('A.*',
        'B.id_asset_kategori',
        'B.nama_kategori',
        'B.nama_sub_kategori',
        'B.nama_komponen')
        ->where('A.id_semisso','=',$id_dbcon)
        ->orderBy('B.nama_kategori','asc')
        ->orderBy('B.nama_sub_kategori','asc')
        ->orderBy('B.nama_komponen','asc')
        ->first();

        return $assetk_con;
    }

    //Get Unique File by Date
    public function get_date_file(){

        //Get Date Now
        $year = strval(Carbon::now('Asia/Jakarta')->year);

        if(Carbon::now('Asia/Jakarta')->month < 10){
            $month = strval(Carbon::now('Asia/Jakarta')->month);
            $month = '0'.$month;
        }
        else{
            $month = strval(Carbon::now('Asia/Jakarta')->month);
        }

        if(Carbon::now('Asia/Jakarta')->day < 10){
            $day = strval(Carbon::now('Asia/Jakarta')->day);
            $day = '0'.$day;
        }
        else{
            $day = strval(Carbon::now('Asia/Jakarta')->day);
        }

        //Get Time Now
        if(Carbon::now('Asia/Jakarta')->hour < 10){
            $hour = strval(Carbon::now('Asia/Jakarta')->hour);
            $hour = '0'.$hour;
        }
        else{
            $hour = strval(Carbon::now('Asia/Jakarta')->hour);
        }

        if(Carbon::now('Asia/Jakarta')->minute < 10){
            $minute = strval(Carbon::now('Asia/Jakarta')->minute);
            $minute = '0'.$minute;
        }
        else{
            $minute = strval(Carbon::now('Asia/Jakarta')->minute);
        }

        if(Carbon::now('Asia/Jakarta')->second < 10){
            $second = strval(Carbon::now('Asia/Jakarta')->second);
            $second = '0'.$second;
        }
        else{
            $second = strval(Carbon::now('Asia/Jakarta')->second);
        }

        $date_now = $year.$month.$day;
        $time_now = $hour.$minute.$second;
        $date_file = $date_now.$time_now;

        return $date_file;
    }

    //Model File Export
    public function list_file_export(){

        $fexport = FileExportM::orderBy('nama_export','asc')
        ->get();

        $koneksidb = $this->get_dbcon_all();

    	return view('masterdata.file_export', compact('fexport','koneksidb'));
    }

    public function insert_file_export(Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_export' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'query_export' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Post Request
        $fexport = FileExportM::create([
            'id_koneksidb' => $layanan[0],
            'nama_layanan' => $layanan[1],
            'nama_export' => $data->nama_export,
            'prefix_name' => $data->prefix_name,
            'sftp_name' => $data->sftp_name,
            'query_export' => $data->query_export,
            'coll_fields' => $data->coll_fields
        ]);

    	return redirect('/masterdata/file_export');
    }

    public function update_file_export($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_export' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'query_export' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Update Request
        $fexport = FileExportM::find($id);
        $fexport->id_koneksidb = $layanan[0];
        $fexport->nama_layanan = $layanan[1];
        $fexport->nama_export = $data->nama_export;
        $fexport->prefix_name = $data->prefix_name;
        $fexport->sftp_name = $data->sftp_name;
        $fexport->query_export = $data->query_export;
        $fexport->coll_fields = $data->coll_fields;
        $fexport->save();

        return redirect('/masterdata/file_export');
    }

    public function delete_file_export($id){

        $fexport = FileExportM::find($id);
        $fexport->delete();

        return redirect('/masterdata/file_export');
    }

    //Model File Import
    public function list_file_import(){

        $fimport = FileImportM::orderBy('nama_import','asc')
        ->get();

        $koneksidb = $this->get_dbcon_all();

    	return view('masterdata.file_import', compact('fimport','koneksidb'));
    }

    public function insert_file_import(Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_import' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'table_source' => 'required',
            'pk_field' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Post Request
        $fimport = FileImportM::create([
            'id_koneksidb' => $layanan[0],
            'nama_layanan' => $layanan[1],
            'nama_import' => $data->nama_import,
            'prefix_name' => $data->prefix_name,
            'sftp_name' => $data->sftp_name,
            'table_source' => $data->table_source,
            'pk_field' => $data->pk_field,
            'coll_fields' => $data->coll_fields
        ]);

    	return redirect('/masterdata/file_import');
    }

    public function update_file_import($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_import' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'table_source' => 'required',
            'pk_field' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Update Request
        $fimport = FileImportM::find($id);
        $fimport->id_koneksidb = $layanan[0];
        $fimport->nama_layanan = $layanan[1];
        $fimport->nama_import = $data->nama_import;
        $fimport->prefix_name = $data->prefix_name;
        $fimport->sftp_name = $data->sftp_name;
        $fimport->table_source = $data->table_source;
        $fimport->pk_field = $data->pk_field;
        $fimport->coll_fields = $data->coll_fields;
        $fimport->save();

        return redirect('/masterdata/file_import');
    }

    public function delete_file_import($id){

        $fimport = FileImportM::find($id);
        $fimport->delete();

        return redirect('/masterdata/file_import');
    }

    public function get_file_import($kode_file){

        $get_file_import = DB::table('tblt_monitoring as A')
        ->join('tblm_file_export as B','A.id_filein','=','B.id_file_export')
        ->where('status_filein','=','Exported')
        ->whereRaw('substring(nama_export for 3) = ?',$kode_file)
        ->orderBy('id_monitoring','desc')
        ->pluck('nama_file','id_monitoring');

        return json_encode($get_file_import);
    }

    //Model File Import UI
    public function list_file_importui(){

        $fimportui = FileImportUIM::orderBy('nama_importui','asc')
        ->get();

        $koneksidb = $this->get_dbcon_all();

    	return view('masterdata.file_importui', compact('fimportui','koneksidb'));
    }

    public function insert_file_importui(Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_importui' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'table_source' => 'required',
            'pk_field' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Post Request
        $fimportui = FileImportUIM::create([
            'id_koneksidb' => $layanan[0],
            'nama_layanan' => $layanan[1],
            'nama_importui' => $data->nama_importui,
            'prefix_name' => $data->prefix_name,
            'sftp_name' => $data->sftp_name,
            'table_source' => $data->table_source,
            'pk_field' => $data->pk_field,
            'coll_fields' => $data->coll_fields
        ]);

    	return redirect('/masterdata/file_import_ui');
    }

    public function update_file_importui($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'nama_layanan' => 'required',
            'nama_importui' => 'required',
            'prefix_name' => 'required',
            'sftp_name' => 'required',
            'table_source' => 'required',
            'pk_field' => 'required',
            'coll_fields' => 'required'
        ]);

        $layanan = explode('|', $data->nama_layanan);

        //Update Request
        $fimportui = FileImportUIM::find($id);
        $fimportui->id_koneksidb = $layanan[0];
        $fimportui->nama_layanan = $layanan[1];
        $fimportui->nama_importui = $data->nama_importui;
        $fimportui->prefix_name = $data->prefix_name;
        $fimportui->sftp_name = $data->sftp_name;
        $fimportui->table_source = $data->table_source;
        $fimportui->pk_field = $data->pk_field;
        $fimportui->coll_fields = $data->coll_fields;
        $fimportui->save();

        return redirect('/masterdata/file_import_ui');
    }

    public function delete_file_importui($id){

        $fimportui = FileImportUIM::find($id);
        $fimportui->delete();

        return redirect('/masterdata/file_import_ui');
    }

    public function get_file_importui($kode_file){

        $get_file_importui = '';

        return json_encode($get_file_importui);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MonitoringController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $monak = DB::table('tblt_monitoring as A')
        ->leftJoin('tblm_file_export as B','A.id_filein','=','B.id_file_export')
        ->leftJoin('tblm_file_import as C','A.id_filein_import','=','C.id_file_import')
        ->leftJoin('tblm_file_importui as D','A.id_filein_import','=','D.id_file_importui')
        ->select('A.*',
        'B.nama_layanan',
        'B.nama_export',
        'B.sftp_name',
        'C.nama_layanan as nama_layanan_import',
        'C.nama_import',
        'C.prefix_name',
        'C.sftp_name as sftp_name_import',
        'D.nama_importui',
        'D.sftp_name as sftp_name_importui')
        ->orderBy('id_monitoring','desc')
        ->get();

        return view('monitoring', compact('monak'));
    }

    public function get_file($sftp_name, $nama_file){

        $get_file = Storage::disk($sftp_name)->download($nama_file);

        return $get_file;
    }
}

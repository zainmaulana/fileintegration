<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\ExportController;

use App\FileExportM;

class ExportMasterVendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:export_master_vendor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export Master Vendor from HK Circle to ERP FTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ExportController;

        //Manual Set ID Export
        // $controller->post_export_cmd(1);

        //Auto Set ID Export
        $get_id = FileExportM::where('prefix_name','=','vendor')
        ->first();
        $controller->post_export_cmd($get_id->id_file_export);

        Log::info('Schedule Export Master Vendor sucessfully run');

        return $this->info('Successfully export Master Vendor CSV to FTP.');
    }
}

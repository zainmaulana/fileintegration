<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\ImportController;

use App\FileImportM;

class ImportMasterCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:import_master_customer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Master Customer from SAP to CRM';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ImportController;

        //Manual Set ID Import
        // $controller->post_import_cmd(1);

        $get_id = FileImportM::where('prefix_name','=','customer')
        ->first();
        $controller->post_import_cmd($get_id->id_file_import);

        Log::info('Schedule Import Master Customer sucessfully run');

        return $this->info('Successfully import Master Customer CSV to CRM.');
    }
}

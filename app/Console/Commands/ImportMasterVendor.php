<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\ImportController;

use App\FileImportM;

class ImportMasterVendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:import_master_vendor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Master Vendor from SAP to HK Circle';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ImportController;

        //Manual Set ID Import
        // $controller->post_import_cmd(1);

        $get_id = FileImportM::where('sftp_name','=','sftp_vendor_suc')
        ->first();
        $controller->post_import_cmd($get_id->id_file_import);

        Log::info('Schedule Import Master Vendor sucessfully run');

        return $this->info('Successfully import Master Vendor CSV to HK Circle.');
    }
}

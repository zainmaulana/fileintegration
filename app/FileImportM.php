<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileImportM extends Model
{
    //
    protected $table = 'tblm_file_import';

    protected $primaryKey = 'id_file_import';

    protected $fillable = ['id_koneksidb',
    'nama_layanan',
    'nama_import',
    'prefix_name',
    'sftp_name',
    'table_source',
    'pk_field',
    'coll_fields'
    ];

    const CREATED_AT = null;
    const UPDATED_AT = 'last_updated_on';
}

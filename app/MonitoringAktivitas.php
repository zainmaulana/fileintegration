<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonitoringAktivitas extends Model
{
    //
    protected $table = 'tblt_monitoring';

    protected $primaryKey = 'id_monitoring';

    protected $fillable = ['id_filein',
    'id_filein_import',
    'nama_file',
    'status_filein'
    ];

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'last_updated_on';
}

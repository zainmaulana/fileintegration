<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileexportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblm_file_export', function (Blueprint $table) {
            $table->bigIncrements('id_file_export');
            $table->integer('id_koneksidb');
            $table->text('nama_layanan')->nullable();
            $table->text('nama_export')->nullable();
            $table->text('prefix_name')->nullable();
            $table->text('sftp_name')->nullable();
            $table->text('query_export')->nullable();
            $table->text('coll_fields')->nullable();
            $table->timestamp('last_updated_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblm_file_export');
    }
}

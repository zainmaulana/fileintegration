<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKoneksidbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblm_koneksidb', function (Blueprint $table) {
            $table->bigIncrements('id_koneksidb');
            $table->text('nama_layanan')->nullable();
            $table->text('name_db_conn')->nullable();
            $table->text('schema_db')->nullable();
            $table->text('server_db')->nullable();
            $table->integer('port_db')->nullable();
            $table->text('app_db_uname')->nullable();
            $table->text('app_db_pass')->nullable();
            $table->timestamp('last_updated_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblm_koneksidb');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMonitoringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblt_monitoring', function (Blueprint $table) {
            $table->bigIncrements('id_monitoring');
            $table->integer('id_filein');
            $table->integer('id_filein_import')->nullable();
            $table->text('nama_file')->nullable();
            $table->text('status_filein')->nullable();
            $table->timestamp('created_on')->nullable();
            $table->timestamp('last_updated_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblt_monitoring');
    }
}

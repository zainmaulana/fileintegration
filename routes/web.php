<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::group(['prefix' => 'admin-fihk'], function () {

    Auth::routes();
});

Route::group(['middleware' => ['auth']], function () {

    //Home
    Route::get('/', 'HomeController@index')->name('home');

    //Modul Exp & Imp
    Route::post('/csv/export', 'ExportController@post_export');
    Route::post('/csv/import', 'ImportController@post_import');
    Route::post('/csv/importui', 'ImportUIController@post_importui');
    Route::post('/csv/import_manual', 'ImportController@post_import_manual');

    //Monitoring
    Route::get('/monitoring', 'MonitoringController@index');
    Route::get('/monitoring/download{sftp_name}/file{nama_file}', 'MonitoringController@get_file');

    //MD - User
    Route::get('/masterdata/user', 'MasterDataController@list_user')->name('user');
    Route::post('/masterdata/user/insert', 'MasterDataController@insert_user');
    Route::post('/masterdata/user/update{id}','MasterDataController@update_user');
    Route::post('/masterdata/user/delete{id}', 'MasterDataController@delete_user');

    //MD - File Export
    Route::get('/masterdata/file_export', 'MasterDataController@list_file_export')->name('file_export');
    Route::post('/masterdata/file_export/insert', 'MasterDataController@insert_file_export');
    Route::post('/masterdata/file_export/update{id}','MasterDataController@update_file_export');
    Route::post('/masterdata/file_export/delete{id}', 'MasterDataController@delete_file_export');

    //MD - File Import
    Route::get('/masterdata/file_import', 'MasterDataController@list_file_import')->name('file_import');
    Route::post('/masterdata/file_import/insert', 'MasterDataController@insert_file_import');
    Route::post('/masterdata/file_import/update{id}','MasterDataController@update_file_import');
    Route::post('/masterdata/file_import/delete{id}', 'MasterDataController@delete_file_import');
    Route::get('/masterdata/file_import/get/{kode_file}', 'MasterDataController@get_file_import');

    //MD - File Import UI
    Route::get('/masterdata/file_import_ui', 'MasterDataController@list_file_importui')->name('file_import_ui');
    Route::post('/masterdata/file_import_ui/insert', 'MasterDataController@insert_file_importui');
    Route::post('/masterdata/file_import_ui/update{id}','MasterDataController@update_file_importui');
    Route::post('/masterdata/file_import_ui/delete{id}', 'MasterDataController@delete_file_importui');
    Route::get('/masterdata/file_import_ui/get/{kode_file}', 'MasterDataController@get_file_importui');
});

@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Master Data File Import</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data</button>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No. Urut</th>
                                        <th>Opsi</th>
                                        <th>Nama Import</th>
                                        <th>Nama Layanan</th>
                                        <th>Nama File Sumber</th>
                                        <th>Fields</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($fimport as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>
                                            <center>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editData_Update{{ $p->id_file_import }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#editData_Delete{{ $p->id_file_import }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </center>
                                        </td>
                                        <td>{{ $p->nama_import }}</td>
                                        <td>{{ $p->nama_layanan }}</td>
                                        <td>{{ $p->prefix_name }}</td>
                                        <td>{{ $p->coll_fields }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                        <div id="addData" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/file_import/insert" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="nama_layanan">Nama Layanan</label>
                                                    <select class="form-control" name="nama_layanan" required>
                                                        @foreach($koneksidb as $u)
                                                            <option value="{{ $u->id_semisso.'|'.$u->nama_sub_kategori }}">
                                                                {{ $u->nama_sub_kategori }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="nama_import">Nama Import</label>
                                                    <input type="text" class="form-control" id="nama_import" name="nama_import" value="{{ old('nama_import') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="prefix_name">Nama File Sumber</label>
                                                    <input type="text" class="form-control" id="prefix_name" name="prefix_name" value="{{ old('prefix_name') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="sftp_name">Nama SFTP</label>
                                                    <input type="text" class="form-control" id="sftp_name" name="sftp_name" value="{{ old('sftp_name') }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="table_source">Tabel Tujuan</label>
                                                    <input type="text" class="form-control" id="table_source" name="table_source" value="{{ old('table_source') }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="pk_field">Field Matching</label>
                                                    <input type="text" class="form-control" id="pk_field" name="pk_field" value="{{ old('pk_field') }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="coll_fields">Koleksi Field</label>
                                                    <textarea class="form-control" name="coll_fields" id="coll_fields" rows="3">{{ old('coll_fields') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @foreach($fimport as $u)
                        <div id="editData_Update{{ $u->id_file_import }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/file_import/update{{ $u->id_file_import }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="nama_layanan">Nama Layanan</label>
                                                    <select class="form-control" name="nama_layanan" required>
                                                        <option value="{{ $u->id_koneksidb.'|'.$u->nama_layanan }}">{{ $u->nama_layanan }}</option>
                                                        @foreach($koneksidb as $p)
                                                            <option value="{{ $p->id_semisso.'|'.$p->nama_sub_kategori }}">
                                                                {{ $p->nama_sub_kategori }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="nama_import">Nama Import</label>
                                                    <input type="text" class="form-control" id="nama_import" name="nama_import" value="{{ $u->nama_import }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="prefix_name">Nama File Sumber</label>
                                                    <input type="text" class="form-control" id="prefix_name" name="prefix_name" value="{{ $u->prefix_name }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="sftp_name">Nama SFTP</label>
                                                    <input type="text" class="form-control" id="sftp_name" name="sftp_name" value="{{ $u->sftp_name }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="table_source">Tabel Tujuan</label>
                                                    <input type="text" class="form-control" id="table_source" name="table_source" value="{{ $u->table_source }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="pk_field">Field Matching</label>
                                                    <input type="text" class="form-control" id="pk_field" name="pk_field" value="{{ $u->pk_field }}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="coll_fields">Koleksi Field</label>
                                                    <textarea class="form-control" name="coll_fields" id="coll_fields" rows="3">{{ $u->coll_fields }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($fimport as $u)
                        <div id="editData_Delete{{ $u->id_file_import }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-lg modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/file_import/delete{{ $u->id_file_import }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Hapus Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <input type="hidden" class="form-control" name="id_file_import" value="{{ $u->id_file_import }}">
                                            <p>Apakah Anda yakin akan menghapus data ini?</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

        <script type="text/javascript">
            $(document).ready(function() {
                $('#table_id').DataTable( {
                    "scrollX": true
                } );
            } );
        </script>
@endsection

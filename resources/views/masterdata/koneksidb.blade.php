@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Master Data Koneksi DB</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data</button>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No. Urut</th>
                                        <th>Opsi</th>
                                        <th>Nama Layanan</th>
                                        <th>Server Database</th>
                                        <th>Port Database</th>
                                        <th>Tabel User Aplikasi</th>
                                        <th>Nama DB Aplikasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($koneksidb as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>
                                            <center>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editData_Update{{ $p->id_koneksidb }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#editData_Delete{{ $p->id_koneksidb }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </center>
                                        </td>
                                        <td>{{ $p->nama_layanan }}</td>
                                        <td>{{ $p->server_db }}</td>
                                        <td>{{ $p->port_db }}</td>
                                        <td>{{ $p->model_name_db }}</td>
                                        <td>{{ $p->name_db_conn }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                        <div id="addData" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/koneksidb/insert" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="nama_layanan">Nama Layanan</label>
                                                    <select class="form-control" name="nama_layanan" required>
                                                        @foreach($assetk as $u)
                                                            <option value="{{ $u->nama_kategori }} -
                                                                {{ $u->nama_sub_kategori }}
                                                                @if($u->nama_komponen <> NULL)
                                                                - {{ $u->nama_komponen }}
                                                                @endif">
                                                                {{ $u->nama_kategori }} -
                                                                {{ $u->nama_sub_kategori }}
                                                                @if($u->nama_komponen <> NULL)
                                                                - {{ $u->nama_komponen }}
                                                                @endif
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="schema_db">Skema Database</label>
                                                    <input type="text" class="form-control" id="schema_db" name="schema_db" value="{{ old('schema_db') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="server_db">Server Database</label>
                                                    <input type="text" class="form-control" id="server_db" name="server_db" value="{{ old('server_db') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="port_db">Port Database</label>
                                                    <input type="text" class="form-control" id="port_db" name="port_db" value="{{ old('port_db') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name_db_conn">Nama DB Aplikasi</label>
                                                    <input type="text" class="form-control" id="name_db_conn" name="name_db_conn" value="{{ old('name_db_conn') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="app_db_uname">Username DB</label>
                                                    <input type="text" class="form-control" id="app_db_uname" name="app_db_uname" value="{{ old('app_db_uname') }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name_db_conn">Password DB</label>
                                                    <input type="password" class="form-control" id="app_db_pass" name="app_db_pass" value="{{ old('app_db_pass') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @foreach($koneksidb as $u)
                        <div id="editData_Update{{ $u->id_koneksidb }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/koneksidb/update{{ $u->id_koneksidb }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="nama_layanan">Nama Layanan</label>
                                                    <select class="form-control" name="nama_layanan" required>
                                                        <option value="{{ $u->nama_kategori }} -
                                                            {{ $u->nama_sub_kategori }}
                                                            @if($u->nama_komponen <> NULL)
                                                            - {{ $u->nama_komponen }}
                                                            @endif">
                                                            {{ $u->nama_layanan }}
                                                        </option>
                                                        @foreach($assetk as $p)
                                                            <option value="{{ $p->id_asset_kategori }}">
                                                                {{ $p->nama_kategori }} -
                                                                {{ $p->nama_sub_kategori }}
                                                                @if($p->nama_komponen <> NULL)
                                                                - {{ $p->nama_komponen }}
                                                                @endif
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="schema_db">Skema Database</label>
                                                    <input type="text" class="form-control" id="schema_db" name="schema_db" value="{{ $u->schema_db }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="server_db">Server Database</label>
                                                    <input type="text" class="form-control" id="server_db" name="server_db" value="{{ $u->server_db }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="port_db">Port Database</label>
                                                    <input type="text" class="form-control" id="port_db" name="port_db" value="{{ $u->port_db }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name_db_conn">Nama DB Aplikasi</label>
                                                    <input type="text" class="form-control" id="name_db_conn" name="name_db_conn" value="{{ $u->name_db_conn }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="app_db_uname">Username DB</label>
                                                    <input type="text" class="form-control" id="app_db_uname" name="app_db_uname" value="{{ $u->app_db_uname }}">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <label for="name_db_conn">Password DB</label>
                                                    <input type="password" class="form-control" id="app_db_pass" name="app_db_pass" value="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($koneksidb as $u)
                        <div id="editData_Delete{{ $u->id_koneksidb }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/koneksidb/delete{{ $u->id_koneksidb }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Hapus Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <input type="hidden" class="form-control" name="id_koneksidb" value="{{ $u->id_koneksidb }}">
                                            <p>Apakah Anda yakin akan menghapus data ini?</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

        <script type="text/javascript">
            $(document).ready(function() {
                $('#table_id').DataTable( {
                    "scrollX": true
                } );
            } );
        </script>
@endsection

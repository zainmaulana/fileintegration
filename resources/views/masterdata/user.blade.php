@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Master Data User</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data</button>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($user as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>{{ $p->name }}</td>
                                        <td>{{ $p->email }}</td>
                                        <td>
                                            <center>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editData_Update{{ $p->id }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#editData_Delete{{ $p->id }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </center>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                        <div id="addData" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/user/insert" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="name">Nama User</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm-password">Konfirmasi Password</label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @foreach($user as $u)
                        <div id="editData_Update{{ $u->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/user/update{{ $u->id }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="name">Nama User</label>
                                            <input type="text" class="form-control" name="name" value="{{ $u->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" value="{{ $u->email }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm-password">Konfirmasi Password</label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($user as $u)
                        <div id="editData_Delete{{ $u->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/user/delete{{ $u->id }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Hapus Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <input type="hidden" class="form-control" name="id" value="{{ $u->id }}">
                                            <p>Apakah Anda yakin akan menghapus data ini?</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

        <script type="text/javascript">
            $(document).ready(function() {
                $('#table_id').DataTable( {
                    "scrollX": true
                } );
            } );
        </script>
@endsection

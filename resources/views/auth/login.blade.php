@extends('layouts.app_login')
@section('content')
<!--Login-Section-->
<section class="sptb">
    <div class="container-fuild">
        <div class="header-text slide-header-text mt-0 mb-0">
            <div class="text-center">
                <h1 class="mb-1 d-none d-md-block" style="color: #ec1e24;">File Integration</h1>
                <h2 class="mb-1 d-none d-md-block" style="color: #ec1e24;">Hutama Karya</h2>
                <!-- <img src="../images/hk_logo.png" width="25%" height="25%"> -->
            </div>
        </div>
    </div>
</section>
<section class="sptb">
    <div class="container customerpage">
        <div class="row">
            <div class="single-page">
                <div class="col-lg-5 col-xl-4 col-md-6 d-block mx-auto">
                    <div class="wrapper wrapper2">
                        <form id="login" class="card-body" tabindex="500" method="POST" action="{{ route('login') }}">
                            @csrf
                            <h2 style="color: #353434;">Login</h2>
                            <div class="mail">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <label>Mail or Username</label>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="passwd">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <label>Password</label>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="submit">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>
                            <!-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot/Reset Your Password?') }}
                                </a>
                            @endif -->
                            <p class="text-dark mb-0">Lupa Password ?<a href="https://servicedesk.hutamakarya.com/view_email_hk" class="text-primary ml-1">Klik Disini</a></p>
                        </form>
                        <div class="card-body">
                            <div class="text-center">
                                <a href="https://www.hutamakarya.com/"><i class="fa fa-home mr-3 text-primary"></i> PT. Hutama Karya (Persero)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/Login-Section-->
@endsection

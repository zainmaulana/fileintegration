<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>File Integration HK</title>

    <!-- Favicon -->
	<link rel="icon" type="image/x-icon" href="../images/hk_logo.png"/>
	<link rel="shortcut icon" type="image/x-icon" href="../images/hk_logo.png" />

    @include('includes.main_js')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">

    @include('includes.main_css')
</head>
<body>
    <div class="wrapper" id="app">
        @include('components.sidebar')

        <div id="content">
            @include('components.navbar')

            <main>
                @include('includes.flash-message')
                @yield('content')
            </main>
        </div>
    </div>
</body>

<script tabindex="text/javascript">
    $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
</script>

</html>
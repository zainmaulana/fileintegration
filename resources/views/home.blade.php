@extends('layouts.app_custom')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Export Manual</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="/csv/export" method="post">

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="id_file_export" required>
                                        @foreach($fexport as $u)
                                            <option value="{{ $u->id_file_export }}">
                                                {{ $u->nama_export }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" class="btn btn-success">Export CSV</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Import Manual</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="/csv/import" method="post">

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="id_file_import" required>
                                        @foreach($fimport as $u)
                                            <option value="{{ $u->id_file_import }}">
                                                {{ $u->nama_import }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" class="btn btn-success">Import CSV</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Import UI Manual</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="/csv/importui" method="post">

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <select class="form-control" name="id_file_importui" required>
                                        @foreach($fimportui as $u)
                                            <option value="{{ $u->id_file_importui }}">
                                                {{ $u->nama_importui }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" class="btn btn-success">Import CSV</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header">Import Manual (Search File)</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="/csv/import_manual" method="post">

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="id_file_import2" required>
                                        <option>Pilih Import</option>
                                        @foreach($fimport2 as $u)
                                            <option value="{{ $u->id_file_import.'|'.substr($u->nama_import,0,3) }}">
                                                {{ $u->nama_import }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" id="file_import_search" name="file_import_search" required>
                                        <option value="">Pilih File</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <center>
                                        <button type="submit" class="btn btn-success">Import CSV</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
		jQuery(document).ready(function ()
		{
				jQuery('select[name="id_file_import2"]').on('change',function(){
				var id_import = jQuery(this).val();
                var id_import_arr = id_import.split('|');
				if(id_import_arr[1])
				{
					jQuery.ajax({
						url : '/masterdata/file_import/get/' + id_import_arr[1],
						type : "GET",
						dataType : "json",
						success:function(data)
						{
							console.log(data);
							jQuery('select[name="file_import_search"]').empty();
							jQuery.each(data, function(key,value){
							$('select[name="file_import_search"]').append('<option value="'+ value +'">'+ value +'</option>');
							});
						}
					});
				}
				else
				{
					$('select[name="file_import_search"]').empty();
				}
				});
		});
    </script>

@endsection

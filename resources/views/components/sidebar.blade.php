<nav id="sidebar">
    <div class="sidebar-header">
        <center>
            <a class="navbar-brand" href="{{ url('/') }}">
                <h3>{{ config('app.name', 'FIHK') }}</h3>
            </a>
        </center>
    </div>

    <ul class="list-unstyled components">
        <li>
            <a href="/"><i class="fa fa-home"></i>&emsp; Home</a>
        </li>
        <li>
            <a href="/monitoring"><i class="fa fa-desktop"></i>&emsp; Monitoring</a>
        </li>
        <li>
            <a href="#masterdata" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fa fa-database" aria-hidden="true"></i>&emsp; Master Data
            </a>
            <ul class="collapse list-unstyled" id="masterdata">
                <li>
                    <a href="/masterdata/user">User</a>
                </li>
                <li>
                    <a href="/masterdata/file_export">File Export</a>
                </li>
                <li>
                    <a href="/masterdata/file_import">File Import</a>
                </li>
                <li>
                    <a href="/masterdata/file_import_ui">File Import UI</a>
                </li>
            </ul>
        </li>
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <center>
                &copy; 2021 - Divisi Sistem, IT & Riset Teknologi
            </center>
        </li>
    </ul>
</nav>
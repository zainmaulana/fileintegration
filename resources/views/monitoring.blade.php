<?php
    use Carbon\Carbon;
?>

@extends('layouts.app_custom')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Monitoring Transaksi Integrasi</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Opsi</th>
                                        <th>Tanggal Dibuat</th>
                                        <th>Tanggal Diperbarui</th>
                                        <th>Nama Layanan Export</th>
                                        <th>Nama Layanan Import</th>
                                        <th>Nama File Export</th>
                                        <th>Nama File Import</th>
                                        <th>Status</th>
                                        <th>Nama Export</th>
                                        <th>Nama Import</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($monak as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>
                                            <center>
                                                @if($p->status_filein == 'Imported Only')
                                                    <?php
                                                        $sftp_name = explode(',',$p->sftp_name_importui);
                                                    ?>

                                                    <a class="btn btn-success" href="/monitoring/download{{ $sftp_name[1] }}/file{{ $p->nama_file }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> File Import</a>
                                                @else
                                                    <a class="btn btn-primary" href="/monitoring/download{{ $p->sftp_name }}/file{{ $p->nama_file }}" target="_blank">
                                                    <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> File Export</a>
                                                    @if($p->id_filein_import <> NULL)
                                                        @if(strpos($p->prefix_name,"-"))
                                                            <a class="btn btn-success" href="/monitoring/download{{ $p->sftp_name_import }}/file{{ $p->prefix_name.$p->nama_file }}" target="_blank">
                                                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> File Import</a>
                                                        @else
                                                            <a class="btn btn-success" href="/monitoring/download{{ $p->sftp_name_import }}/file{{ $p->nama_file }}" target="_blank">
                                                            <i class="fa fa-arrow-circle-down" aria-hidden="true"></i> File Import</a>
                                                        @endif
                                                    @endif
                                                @endif
                                            </center>
                                        </td>
                                        <td>
                                            <?php
                                                $created_on = Carbon::parse($p->created_on)->addHours(7);
                                            ?>
                                            {{ $created_on }}
                                        </td>
                                        <td>{{ $p->last_updated_on }}</td>
                                        <td>{{ $p->nama_layanan }}</td>
                                        <td>{{ $p->nama_layanan_import }}</td>
                                        <td>
                                            @if($p->status_filein == 'Imported Only')
                                                -
                                            @else
                                                {{ $p->nama_file }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($p->id_filein_import == NULL)
                                                -
                                            @else
                                                @if($p->status_filein == 'Imported Only')
                                                    {{ $p->nama_file }}
                                                @else
                                                    @if(strpos($p->prefix_name,"-"))
                                                        {{ $p->prefix_name.$p->nama_file }}
                                                    @else
                                                        {{ $p->nama_file }}
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <center>
                                                @if($p->status_filein == 'Imported Only')
                                                    <span class="badge badge-primary" style="background-color: #00bfa5;">Imported Only</span>
                                                @else
                                                    @if($p->status_filein == 'Exported')
                                                        <span class="badge badge-primary" style="background-color: #039be5;">Exported</span>
                                                    @elseif($p->status_filein == 'Imported')
                                                        <span class="badge badge-primary" style="background-color: #00bfa5;">Imported</span>
                                                    @endif
                                                @endif
                                            </center>
                                        </td>
                                        <td>{{ $p->nama_export }}</td>
                                        <td>
                                            @if($p->status_filein == 'Imported Only')
                                                {{ $p->nama_importui }}
                                            @else
                                                {{ $p->nama_import }}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );
	</script>
@endsection